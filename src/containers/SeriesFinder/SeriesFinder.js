import React, {useEffect, useReducer} from 'react';
import {CURRENT_SERIES, FINDING_SERIES, INFO_SERIES} from "../../store/actionTypes";
import instance from "../../axios-series";
import {NavLink} from "react-router-dom";

const initialState = {
    series: "",
    findSeries: null,
    infoSeries: null,
    error: null
};

const reducer = (state, action) => {
    switch (action.type) {
        case CURRENT_SERIES:
            return {...state, series: action.value}
        case FINDING_SERIES:
            return {...state, findSeries: action.value}
        case INFO_SERIES:
            return {...state, infoSeries: action.value}
        default:
            return state;
    };
};

export const currentSeries = value => {
    return {type: CURRENT_SERIES, value}
};

export const findingSeries = value => {
    return {type: FINDING_SERIES, value}
};

export const infoSeries = (value) => {
    return {type: INFO_SERIES, value}
};

const SeriesFinder = () => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const onChangeSeries = (event) => {
        const value = event.target.value;
        dispatch(currentSeries(value));
    };

    useEffect(() => {
        const fetchData = async () => {
            const response = await instance.get(state.series);
            dispatch(findingSeries(response.data));
        };
        fetchData().catch(console.error);
    }, [state.series])

    let result = [];

    if (state.findSeries !== null) {
        result = state.findSeries.map(series => {
            return <NavLink to={`/pages/${series.show.id}`} className="list-group-item">{series.show.name}</NavLink>
        })
    };


    return (
        <div className="container my-5">
            <div className="form-group row">
                <label htmlFor="inputFinder" className="col-sm-2 col-form-label">Search for TV-shows</label>
                <input type="text" className="form-control" id="inputFinder"
                       onChange={onChangeSeries}
                />
                    <div>
                        {result}
                    </div>
            </div>
        </div>
    );
};

export default SeriesFinder;